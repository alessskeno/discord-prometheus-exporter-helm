FROM golang:1.21.4 as builder

WORKDIR /app

COPY src/ .

RUN go mod download

RUN CGO_ENABLED=0 GOOS=linux go build -o discord-prometheus-exporter .

FROM alpine:latest

WORKDIR /root/

COPY --from=builder /app/discord-prometheus-exporter .

ENTRYPOINT ["./discord-prometheus-exporter"]
