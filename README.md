# Description

<hr>
A Prometheus exporter for monitoring Discord servers, written in Go. This exporter collects various metrics from your Discord server and exposes them in a format that Prometheus can scrape and use for monitoring and alerting.

For effortless visualization of the Discord metrics exported to Prometheus, you can use `dashboard.json` in the
repository to import a Grafana dashboard:

<img width="32%" alt="1" src="https://gitlab.com/alessskeno/discord-prometheus-exporter-helm/-/raw/main/images/image1.png"> <img width="32%" alt="2" src="https://gitlab.com/alessskeno/discord-prometheus-exporter-helm/-/raw/main/images/image2.png"> <img width="32%" alt="3" src="https://gitlab.com/alessskeno/discord-prometheus-exporter-helm/-/raw/main/images/image3.png">

## Table of Contents

- [Installation](#installation)
- [Available Features](#features)
- [Configuration](#configuration)
- [License](#license)

## Installation

<hr>

### Prerequisites

- [Helm](https://helm.sh/docs/intro/install/)
- [Prometheus](https://prometheus.io/docs/introduction/first_steps/)
- [Grafana](https://grafana.com/docs/grafana/latest/getting-started/getting-started-prometheus/)
- [Discord Bot Token](https://discord.com/developers/applications)
- [Kubernetes Cluster](https://kubernetes.io/docs/setup/)

### Add Helm Repository

Add the public Helm repository from your GitLab package registry:

```sh
helm repo add aleskerov https://gitlab.com/api/v4/projects/58080142/packages/helm/stable
helm repo update
```

### Install the Chart

To install the chart with the release name `discord-prometheus-exporter`:

```sh
helm upgrade --install discord-prometheus-exporter aleskerov/discord-prometheus-exporter --set discordBotToken="DISCORD_BOT_TOKEN"
```

## Features

<hr>

- **Members Count:** The number of members in the Discord server.
- **Roles Count:** The number of roles in the Discord server.
- **Active Members Count:** The number of active members in the Discord server.
- **Guild Creation Date:** The creation date of the Discord server.
- **Bot Latency:** The latency of the Discord bot.
- **Voice Channel Users Count:** The number of users in voice channels.
- **Nitro Boost Count:** The number of Nitro boosts in the Discord server.
- **Banned Users:** The number of banned users in the Discord server.

## Exporting to Prometheus

<hr>
For getting your discord server metrics into an existing Prometheus instance, you can use the following prometheus configuration:

```yaml
- job_name: 'discord-exporter'
  scrape_interval: 30s
  scrape_timeout: 30s
  metrics_path: /metrics
  static_configs:
    - targets:
        - 'discord-prometheus-exporter.<namespace>:<service.port>'
```

## Configuration

<hr>

The following table lists the configurable parameters of the Discord Prometheus Exporter chart and their default values.

| Parameter         | Description        | Default |
|-------------------|--------------------|---------|
| `discordBotToken` | Discord bot token  | `""`    |
| `service.port`    | Service port       | `8080`  |
| `replicaCount`    | Number of replicas | `1`     |

## License

<hr>

(c) Kenan Aleskerli, 2024. Licensed under the [MIT](LICENSE) license.
