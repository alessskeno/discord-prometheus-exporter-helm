package main

import (
	"fmt"
	"github.com/bwmarrin/discordgo"
	"os"
	"time"
)

type NitroBoost struct {
	Count int
	Level int
}

const (
	Level1NitroBoost = 2
	Level2NitroBoost = 7
	Level3NitroBoost = 14
)

func getNitroLevel(count int) int {
	switch {
	case count >= Level3NitroBoost:
		return 3
	case count >= Level2NitroBoost:
		return 2
	case count >= Level1NitroBoost:
		return 1
	default:
		return 0
	}
}

var membersCountFromDiscord map[string]int                // [guildName]memberCount
var rolesCountFromDiscord map[string]int                  // [guildName]roleCount
var activeMembersCountFromDiscord map[string]int          // [guildName]activeMemberCount
var guildCreationDateFromDiscord map[string]int           // [guildName]creationDate
var botLatencyFromDiscord int                             // botLatency
var voiceChannelUsersCountFromDiscord map[string]int      // [guildName]voiceChannelUsersCount
var guildNitroBoostCountFromDiscord map[string]NitroBoost // [guildName]nitroBoostCount
var bannedUsersFromDiscord map[string]int                 // [guildName]bannedUsersCount

func connectToDiscord() (*discordgo.Session, error) {
	discord, err := discordgo.New("Bot " + os.Getenv("DISCORD_BOT_TOKEN"))
	if err != nil {
		fmt.Println("Error connecting to discord: ", err)
		return nil, err
	}

	discord.AddHandler(handleReady)
	discord.AddHandler(handleGuildCreate)

	discord.Identify.Intents = discordgo.IntentsAll

	err = discord.Open()
	if err != nil {
		fmt.Println("Error opening connection to discord: ", err)
		return nil, err
	}
	fmt.Println("Successfully connected to discord")
	return discord, nil
}

func updateMetrics(s *discordgo.Session) {
	membersCountFromDiscord = make(map[string]int)
	rolesCountFromDiscord = make(map[string]int)
	activeMembersCountFromDiscord = make(map[string]int)
	guildCreationDateFromDiscord = make(map[string]int)
	botLatencyFromDiscord = int(s.HeartbeatLatency().Milliseconds())
	voiceChannelUsersCountFromDiscord = make(map[string]int)
	guildNitroBoostCountFromDiscord = make(map[string]NitroBoost)
	bannedUsersFromDiscord = make(map[string]int)
	for _, guild := range s.State.Guilds {
		// Members count
		membersCountFromDiscord[guild.Name] = guild.MemberCount

		// Roles count
		rolesCountFromDiscord[guild.Name] = len(guild.Roles)

		// Discord bot latency
		creationTime, _ := discordgo.SnowflakeTimestamp(guild.ID)
		daysSinceCreation := int(time.Since(creationTime).Hours() / 24)
		guildCreationDateFromDiscord[guild.Name] = daysSinceCreation

		// Voice channel active users count (only for voice channels)
		voiceChannelUsersCountFromDiscord[guild.Name] = 0
		for _, channel := range guild.Channels {
			if channel.Type == discordgo.ChannelTypeGuildVoice {
				for _, vs := range guild.VoiceStates {
					if vs.ChannelID == channel.ID {
						voiceChannelUsersCountFromDiscord[guild.Name]++
					}
				}
			}
		}

		// Nitro boost count in the guild
		guildNitroBoostCountFromDiscord[guild.Name] = NitroBoost{
			Count: guild.PremiumSubscriptionCount,
			Level: getNitroLevel(guild.PremiumSubscriptionCount),
		}

		bannedUsersFromDiscord[guild.Name] = 0
		// Banned users
		bans, err := s.GuildBans(guild.ID, 1000, "", "")
		if err != nil {
			fmt.Printf("Error fetching bans for guild %s: %s\n", guild.Name, err)
			continue
		}

		var bannedUsers []string
		for _, ban := range bans {
			bannedUsers = append(bannedUsers, ban.User.Username)
		}
		bannedUsersFromDiscord[guild.Name] = len(bannedUsers)

		// Active members count
		activeMembersCountFromDiscord[guild.Name] = 0
		for _, member := range guild.Members {
			presence, _ := s.State.Presence(guild.ID, member.User.ID)
			if presence != nil && presence.Status != discordgo.StatusOffline && !member.User.Bot {
				activeMembersCountFromDiscord[guild.Name]++
			}
		}
	}

	scrapeMetrics()
}

func handleReady(s *discordgo.Session, event *discordgo.Ready) {
	updateMetrics(s)
}

func handleGuildCreate(s *discordgo.Session, event *discordgo.GuildCreate) {
	updateMetrics(s)
}
