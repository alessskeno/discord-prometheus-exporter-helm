package main

import (
	"github.com/bwmarrin/discordgo"
	"time"
)

func main() {
	discord, err := connectToDiscord()
	if err != nil {
		panic(err)
	}

	defer func(discord *discordgo.Session) {
		err := discord.Close()
		if err != nil {

		}
	}(discord)

	startPrometheus()

	ticker := time.NewTicker(20 * time.Second)
	go func() {
		for {
			select {
			case <-ticker.C:
				updateMetrics(discord)
			}
		}
	}()

	<-make(chan struct{})
}
