package main

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"net/http"
	"strconv"
)

var (
	membersCount = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "discord_member_count",
			Help: "Number of members in all guilds",
		},
		[]string{"guild_name"},
	)
	rolesCount = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "discord_role_count",
			Help: "Number of roles in the discord server",
		},
		[]string{"guild_name"},
	)
	activeMembersCount = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "discord_active_member_count",
			Help: "Number of active members in all guilds",
		},
		[]string{"guild_name"},
	)
	guildCreationDate = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "discord_guild_creation_date",
			Help: "Creation date of the guild",
		},
		[]string{"guild_name"},
	)
	voiceChannelUsersCount = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "discord_voice_channel_users_count",
			Help: "Number of voice channel users in the discord server",
		},
		[]string{"guild_name"},
	)
	guildNitroBoostCount = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "discord_guild_nitro_boost_count",
			Help: "Number of nitro boosts in the discord server",
		},
		[]string{"guild_name", "level"},
	)

	bannedUsers = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "discord_banned_users",
			Help: "Banned users in all guilds",
		},
		[]string{"guild_name"},
	)

	botLatency = prometheus.NewGauge(
		prometheus.GaugeOpts{
			Name: "discord_bot_latency",
			Help: "Latency of the bot",
		},
	)
)

func init() {
	prometheus.MustRegister(membersCount)
	prometheus.MustRegister(rolesCount)
	prometheus.MustRegister(activeMembersCount)
	prometheus.MustRegister(guildCreationDate)
	prometheus.MustRegister(botLatency)
	prometheus.MustRegister(voiceChannelUsersCount)
	prometheus.MustRegister(guildNitroBoostCount)
	prometheus.MustRegister(bannedUsers)

}

func scrapeMetrics() {
	membersCount.Reset()
	for guildName, count := range membersCountFromDiscord {
		membersCount.WithLabelValues(guildName).Set(float64(count))
	}

	rolesCount.Reset()
	for guildName, count := range rolesCountFromDiscord {
		rolesCount.WithLabelValues(guildName).Set(float64(count))
	}

	activeMembersCount.Reset()
	for guildName, count := range activeMembersCountFromDiscord {
		activeMembersCount.WithLabelValues(guildName).Set(float64(count))
	}

	guildCreationDate.Reset()
	for guildName, date := range guildCreationDateFromDiscord {
		guildCreationDate.WithLabelValues(guildName).Set(float64(date))
	}

	voiceChannelUsersCount.Reset()
	for guildName, count := range voiceChannelUsersCountFromDiscord {
		voiceChannelUsersCount.WithLabelValues(guildName).Set(float64(count))
	}

	guildNitroBoostCount.Reset()
	for guildName, nitroBoost := range guildNitroBoostCountFromDiscord {
		guildNitroBoostCount.WithLabelValues(guildName, strconv.Itoa(nitroBoost.Level)).Set(float64(nitroBoost.Count))
	}

	bannedUsers.Reset()
	for guildName, count := range bannedUsersFromDiscord {
		bannedUsers.WithLabelValues(guildName).Set(float64(count))
	}

	botLatency.Set(float64(botLatencyFromDiscord))
}

func startPrometheus() {
	http.Handle("/metrics", promhttp.Handler())
	go func() {
		err := http.ListenAndServe(":8080", nil)
		if err != nil {
			panic(err)
		}
	}()
}
